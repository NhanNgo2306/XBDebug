import serial
import time

phone = serial.Serial("/dev/ttyUSB2", 57600, timeout=5)

try:
    command = 'AT+CGDCONT?\r\n'
    phone.write(command.encode())
    time.sleep(1)
    result = phone.readline()
    print(result)
    
    response = ""
    while True:
        line = phone.readline().decode().strip()
        if line == "":
            break
        response += line + "\n"

    print(response)
    print("stop")
finally:
    print("close")
    phone.close()

