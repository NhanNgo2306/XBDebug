import serial
import time

phone = serial.Serial("/dev/ttyUSB2", 57600, timeout=5)

try:

    apn_command = 'AT+CGDCONT=1,"IP","m-wap"\r\n'
    phone.write(apn_command.encode())
    time.sleep(1)  

    check_apn_command = 'AT+CGDCONT?\r\n'
    phone.write(check_apn_command.encode())
    result = phone.readline()
    print(result)

    response = ""
    while True:
        line = phone.readline().decode().strip()
        if line == "":
            break
        response += line + "\n"

    # In phản hồi
    print(response)
    print("stop")
finally:
    print("close")
    phone.close()

