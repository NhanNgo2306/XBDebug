import serial
import time

phone = serial.Serial("/dev/ttyUSB2", 57600, timeout=5)

try:
    # Gửi lệnh AT+QENG="servingcell"
    command = 'AT+QENG="servingcell"\r\n'
    phone.write(command.encode())
    time.sleep(1)
    result = phone.readline()
    print(result)
    # Đọc phản hồi từ cổng COM
    response = ""
    while True:
        line = phone.readline().decode().strip()
        if line == "":
            break
        response += line + "\n"

    # In phản hồi
    print(response)
    print("stop")
finally:
    print("close")
    phone.close()
